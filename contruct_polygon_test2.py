from construct_json_test import GenerateJSON

images = []
annotations = []
categories = []

# DESCRIPTION
description = "test"

# IMAGES
image1 = {
    "id": "imagesId",
    "width": "imagesWidth",
    "height": "imagesHeight",
    "file_name": "imagesFile_name"
}

image2 = {
    "id": "imagesId2",
    "width": "imagesWidth2",
    "height": "imagesHeight2",
    "file_name": "imagesFile_name2"
}

images.append(image1)
images.append(image2)


# ANNOTATIONS
annotation1 = {
    "id": "annotations",
    "iscrowd": "annotations",
    "image_id": "annotations",
    "category_id": "annotations",
    "segmentation": [
        [
        683.8709677419355,
        670.9677419354838
        ]
    ],
    "bbox": [
        361.2903225806451,
        223.6559139784946,
        322.58064516129036,
        447.3118279569892
    ],
    "area": "area_sample"
}

annotation2 = {
    "id": "annotations2",
    "iscrowd": "annotations2",
    "image_id": "annotations2",
    "category_id": "annotations2",
    "segmentation": [
        [
        683.8709677419355,
        670.9677419354838
        ]
    ],
    "bbox": [
        361.2903225806451,
        223.6559139784946,
        322.58064516129036,
        447.3118279569892
    ],
    "area": "area_sample2"
}

annotations.append(annotation1)
annotations.append(annotation2)

# CATEGORIES
category1 = {
    "id": 1,
    "name": "male"
}

category2 = {
    "id": 2,
    "name": "female"
}

categories.append(category1)
categories.append(category2)

print(GenerateJSON.generate_json(description,images,annotations,categories))