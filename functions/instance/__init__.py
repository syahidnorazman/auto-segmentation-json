import cv2
import numpy as np
import random
import os
import math
from .mask_rcnn import MaskRCNN
from .config import Config
import colorsys
import time
from datetime import datetime
import imantics
from imantics import Polygons, Mask
import tensorflow as tf
from pathlib import Path
from .construct_label_coco import GenerateJSON
import json
import ast

class configuration(Config):
    NAME = "configuration"

coco_config = configuration(BACKBONE = "resnet101",  NUM_CLASSES =  81,  class_names = ["BG"], IMAGES_PER_GPU = 1, 
DETECTION_MIN_CONFIDENCE = 0.7,IMAGE_MAX_DIM = 1024, IMAGE_MIN_DIM = 800,IMAGE_RESIZE_MODE ="square",  GPU_COUNT = 1) 
        


#############################################################
#############################################################
""" CLASS CUSTOMIZED BY ET FOR COCO MODEL """
#############################################################
#############################################################



class instance_segmentation_custom():
    def __init__(self, infer_speed = None):
        if infer_speed == "average":
            coco_config.IMAGE_MAX_DIM = 512
            coco_config.IMAGE_MIN_DIM = 512
            coco_config.DETECTION_MIN_CONFIDENCE = 0.45

        elif infer_speed == "fast":
            coco_config.IMAGE_MAX_DIM = 384
            coco_config.IMAGE_MIN_DIM = 384
            coco_config.DETECTION_MIN_CONFIDENCE = 0.25

        elif infer_speed == "rapid":
            coco_config.IMAGE_MAX_DIM = 256
            coco_config.IMAGE_MIN_DIM = 256
            coco_config.DETECTION_MIN_CONFIDENCE = 0.20   
            
        coco_config.class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']

        self.model_dir = os.getcwd()

    def load_model(self, model_path, confidence= None):

        if confidence is not None:
            coco_config.DETECTION_MIN_CONFIDENCE = confidence

        self.model = MaskRCNN(mode = "inference", model_dir = self.model_dir, config = coco_config)

        self.model.load_weights(model_path, by_name= True)

        
    
    def select_target_classes(self,BG = False, person=False, bicycle=False, car=False, motorcycle=False, airplane=False,
                      bus=False, train=False, truck=False, boat=False, traffic_light=False, fire_hydrant=False,
                      stop_sign=False,
                      parking_meter=False, bench=False, bird=False, cat=False, dog=False, horse=False, sheep=False,
                      cow=False, elephant=False, bear=False, zebra=False,
                      giraffe=False, backpack=False, umbrella=False, handbag=False, tie=False, suitcase=False,
                      frisbee=False, skis=False, snowboard=False,
                      sports_ball=False, kite=False, baseball_bat=False, baseball_glove=False, skateboard=False,
                      surfboard=False, tennis_racket=False,
                      bottle=False, wine_glass=False, cup=False, fork=False, knife=False, spoon=False, bowl=False,
                      banana=False, apple=False, sandwich=False, orange=False,
                      broccoli=False, carrot=False, hot_dog=False, pizza=False, donut=False, cake=False, chair=False,
                      couch=False, potted_plant=False, bed=False,
                      dining_table=False, toilet=False, tv=False, laptop=False, mouse=False, remote=False,
                      keyboard=False, cell_phone=False, microwave=False,
                      oven=False, toaster=False, sink=False, refrigerator=False, book=False, clock=False, vase=False,
                      scissors=False, teddy_bear=False, hair_dryer=False,
                      toothbrush=False):

        detected_classes = {}
        target_class_names = [BG, person, bicycle, car, motorcycle, airplane,
                        bus, train, truck, boat, traffic_light, fire_hydrant, stop_sign,
                        parking_meter, bench, bird, cat, dog, horse, sheep, cow, elephant, bear, zebra,
                        giraffe, backpack, umbrella, handbag, tie, suitcase, frisbee, skis, snowboard,
                        sports_ball, kite, baseball_bat, baseball_glove, skateboard, surfboard, tennis_racket,
                        bottle, wine_glass, cup, fork, knife, spoon, bowl, banana, apple, sandwich, orange,
                        broccoli, carrot, hot_dog, pizza, donut, cake, chair, couch, potted_plant, bed,
                        dining_table, toilet, tv, laptop, mouse, remote, keyboard, cell_phone, microwave,
                        oven, toaster, sink, refrigerator, book, clock, vase, scissors, teddy_bear, hair_dryer,
                        toothbrush]
        class_names = ["BG", "person", "bicycle", "car", "motorcycle", "airplane",
                         "bus", "train", "truck", "boat", "traffic light", "fire hydrant", "stop sign",
                         "parking meter", "bench", "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear",
                         "zebra",
                         "giraffe", "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee", "skis",
                         "snowboard",
                         "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard",
                         "tennis racket",
                         "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl", "banana", "apple", "sandwich",
                         "orange",
                         "broccoli", "carrot", "hot dog", "pizza", "donut", "cake", "chair", "couch", "potted plant",
                         "bed",
                         "dining table", "toilet", "tv", "laptop", "mouse", "remote", "keyboard", "cell phone",
                         "microwave",
                         "oven", "toaster", "sink", "refrigerator", "book", "clock", "vase", "scissors", "teddy bear",
                         "hair dryer",
                         "toothbrush"]

        for target_class_name, class_name in zip(target_class_names, class_names):
            if (target_class_name == True):
                detected_classes[class_name] = "valid"
            else:
                detected_classes[class_name] = "invalid"
            
        return detected_classes


    def filter_objects(self, segvalues, segment_target_classes):
        """ Code to filter out unused detections and detect specific classes """
        
        bboxes = segvalues['rois']
        scores = segvalues['scores']
        masks = segvalues['masks']
        class_ids = segvalues['class_ids']
            
            
        com_bboxes = []
        com_masks = []
        com_scores = []
        com_class_ids = []
            
        final_dict = []
        for a, b in enumerate(segvalues['class_ids']):
            name = coco_config.class_names[b]

                
            box = bboxes[a]
               
            ma = masks[:, :, a]
                
            score = scores[a]
                
            c_ids = class_ids[a]
                
                
            if (segment_target_classes[name] == "invalid"):
                continue
                    
            com_bboxes.append(box)
            com_class_ids.append(c_ids)
            com_masks.append(ma)
            com_scores.append(score)
                
                 
        final_bboxes = np.array(com_bboxes)
            
        final_class_ids = np.array(com_class_ids)
        final_masks = np.array(com_masks)
        if len(final_masks != 0):
            final_masks = np.stack(final_masks, axis = 2)
            
        final_scores = np.array(com_scores)
            
        final_dict.append({
            "rois": final_bboxes,
            "class_ids": final_class_ids,
            "scores": final_scores,
            "masks": final_masks,})
        
        final_values = final_dict[0]   
        
        
        return final_values    


    def segmentFrame(self, frame, show_bboxes = False,  segment_target_classes = None, extract_segmented_objects = False,
    text_thickness = 0,text_size = 0.6, box_thickness = 2, save_extracted_objects = False,mask_points_values = False,  
    output_image_name = None, verbose = None):


        new_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        # Run detection
        if verbose is not None:
            print("Processing image...")
        results = self.model.detect([new_frame])    

        r = results[0] 

        current_json_list = {}
        current_json_list = get_json_list()
        #annotations = []
        #images = []
        annotations = current_json_list['annotations']
        images = current_json_list['images']

        if len(current_json_list['images']) == 0:
            image_id = 1
        else:
            if isinstance(current_json_list['images'][-1]['id'], int):
                image_id = current_json_list['images'][-1]['id'] + 1
            else:
                exit("Error with image_id datatype")

        h, w, _ = frame.shape

        image = {
            "id": image_id,
            "width": w,
            "height": h,
            "file_name": "imagesFile_name_with_format"
        }
        images.append(image)

        for i in range(r['masks'].shape[-1]):
            mask = r['masks'][:, :, i]

            mask_coordinates = get_mask_coordinates_arr(mask)
            mask_area = get_mask_area(frame,mask,h,w)
            x_top_left, y_top_left, x_length, y_height = get_mask_bbox(r['rois'][i])
            append_annotation(annotations,mask_coordinates,x_top_left,y_top_left,x_length,y_height,mask_area,image_id)

            """
            annotation = {
                "id": "NeedToGetFromJSONList_int",
                "iscrowd": 0,
                "image_id": "NeedToGetFromJSONList_int",
                "category_id": 1,
                "segmentation": [mask_coordinates],
                "bbox": [
                    x_top_left,
                    y_top_left,
                    x_length,
                    y_height
                ],
                "area": mask_area
            }
            annotations.append(annotation)

            
            roi = r['rois'][i]
            print("x1: " + str(roi[1]))
            print("x2: " + str(roi[3]))
            print("y1: " + str(roi[0]))
            print("y2: " + str(roi[2]))
            image = cv2.rectangle(frame, (roi[1], roi[0]), (roi[3], roi[2]), (255, 0, 0), 3)
            cv2.imshow('Instance Segmentation', image)
            cv2.waitKey(0)
            exit()
            """

        #print(GenerateJSON.generate_json(images,annotations))
        #print(polygon_annotation_json(mask_area), file=open('F:/Programming Project/7. Auto Image Extract/test.json', 'w'))

        #print(GenerateJSON.generate_json(images,annotations), file=open('F:/Programming Project/7. Auto Image Extract/empty.json', 'w'))
        print(json.dumps(ast.literal_eval(str(current_json_list))), file=open('F:/Programming Project/7. Auto Image Extract/empty.json', 'w'))
        return True
        print(your_data,  file=open('F:/Programming Project/7. Auto Image Extract/log.txt', 'w'))
        exit()

        your_data = r['masks'].tolist()[0]
        print(your_data,  file=open('F:/Programming Project/7. Auto Image Extract/log.txt', 'w'))
        exit()

        # START ADDED BY ET
        i = 0
        for roi_person in r['rois']:
            x1 = roi_person[1]; 
            x2 = roi_person[3]; 
            y1 = roi_person[0]; 
            y2 = roi_person[2]; 
            roi_test = frame[y1:y2, x1:x2]
            
            '''
            frameHeight, frameWidth, _ = roi_test.shape
            blob = cv2.dnn.blobFromImage(roi_test, 1.0, (300, 300), [104, 117, 123], False, False)

            
            net.setInput(blob)
            detections = net.forward()
            conf_threshold = 0.4

            for i in range(detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > conf_threshold:
                    x1 = int(detections[0, 0, i, 3] * frameWidth)
                    y1 = int(detections[0, 0, i, 4] * frameHeight)
                    x2 = int(detections[0, 0, i, 5] * frameWidth)
                    y2 = int(detections[0, 0, i, 6] * frameHeight)
                    cv2.rectangle(roi_test, (x1, y1), (x2, y2), (255, 0, 0), 3)
            '''

            #cv2.imshow('Instance Segmentation', roi_test)
            #cv2.waitKey(0)
            
            # Image directory
            directory = 'F:/Programming Project/7. Auto Image Extract/img/'

            # Filename
            filename = 'sample{}.jpg'.format(i)
            
            # Using cv2.imwrite() method
            # Saving the image
            # cv2.imwrite(filename, roi_test)
            
            # List files and directories  
            cv2. imwrite(os. path. join(directory , filename), roi_test)
            
            print('{}: Successfully saved'.format(filename))
            i = i+1

        exit()
        # END ADDED BY ET

        """Filter unused detections and detect specific classes """
        
        if segment_target_classes is not None:
            r = self.filter_objects(r, segment_target_classes) 
        
            
        if show_bboxes == False:

            output = display_instances_custom(frame, r['rois'], r['masks'], r['class_ids'], coco_config.class_names)

        else: 
            output = display_box_instances_custom(frame, r['rois'], r['masks'], r['class_ids'], coco_config.class_names, r['scores'],
            text_size = text_size, box_thickness=box_thickness, text_thickness=text_thickness)   

        if output_image_name is not None:
            cv2.imwrite(output_image_name, output)
            print("Processed image saved successfully in your current working directory.")   


            
        
        if extract_segmented_objects == False:
                
            if mask_points_values == True:
                mask = r['masks']
                contain_val = []
                    
                for a in range(mask.shape[2]):
                    m = mask[:,:,a]
                    mask_values = Mask(m).polygons()
                    val = mask_values.points
                    contain_val.append(val)

                contain_val = np.asarray(contain_val, dtype = object)
                r['masks'] = contain_val
             

            return r, output

        else:

            """ Code to extract and crop out each of the objects segmented in an image """
        
            mask = r['masks']
            m = 0
            ex = []
            if len(mask != 0):
                for a in range(mask.shape[2]):
                    
                    ori_frame = new_frame
                    img = cv2.cvtColor(ori_frame, cv2.COLOR_RGB2BGR)
                
                    
                    for b in range(img.shape[2]):
       
                        img[:,:,b] = img[:,:,b] * mask[:,:,a]
                    m+=1
                    extracted_objects = img[np.ix_(mask[:,:,a].any(1), mask[:,:,a].any(0))]
                    ex.append(extracted_objects)
                    if save_extracted_objects == True:
                        save_path = os.path.join("segmented_object" + "_" + str(m) + ".jpg")
                        cv2.imwrite(save_path, extracted_objects)

                extracted_objects = np.array(ex, dtype = object)


                if mask_points_values == True:
                        
                    contain_val = []
                    for a in range(mask.shape[2]):
                        m = mask[:,:,a]
                        mask_values = Mask(m).polygons()
                        val = mask_values.points
                
                        contain_val.append(val)

                    contain_val = np.asarray(contain_val, dtype = object)    
                    r['masks'] = contain_val

                       
                    extract_mask = extracted_objects
                    object_val = []

                    for a in range(extract_mask.shape[2]):
                        m = extract_mask[:,:,a]
                        mask_values = Mask(m).polygons()
                        val = mask_values.points
                        object_val.append(val)

                    object_val = np.asarray(object_val, dtype = object)
                    extracted_objects = object_val

                """ The mask values of each of the extracted cropped object in the image
                is added to the dictionary containing an array of output values:
                """ 

                r.update({"extracted_objects": extracted_objects})
                
            return r, output  




################VISUALIZATION CODE ##################




def random_colors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image

    


def display_instances(image, boxes, masks, class_ids,  class_name):
    
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    
    assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    for i, color in enumerate(colors):
        mask = masks[:, :, i]

        image = apply_mask(image, mask, color)


    return image





def display_box_instances(image, boxes, masks, class_ids, class_name, scores, text_size, box_thickness, text_thickness):
    
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    txt_color = (255,255,255)
    assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    for i, color in enumerate(colors):
        if not np.any(boxes[i]):
            continue

        y1, x1, y2, x2 = boxes[i]
        label = class_name[class_ids[i]]
        score = scores[i] if scores is not None else None
        caption = '{} {:.2f}'.format(label, score) if score else label
        mask = masks[:, :, i]
        
        image = apply_mask(image, mask, color)
        color_rec = [int(c) for c in np.array(colors[i]) * 255]
        image = cv2.rectangle(image, (x1, y1), (x2, y2), color_rec, box_thickness)
        image = cv2.putText(image, caption, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, text_size,  txt_color, text_thickness)
        
    return image




################VISUALIZATION CODE BY ET ##################




def display_instances_custom(image, boxes, masks, class_ids,  class_name):
    
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    
    assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    for i, color in enumerate(colors):
        mask = masks[:, :, i]

        image = apply_mask_custom(image, mask, color)
        cv2.imshow('Instance Segmentation', image)
        cv2.waitKey(0)

    exit()
    return image

'''
def apply_mask_custom(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image
'''

def apply_mask_custom(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                    cv2.GaussianBlur(image[:, :, c],(41,41),0),
                                    image[:, :, c])

    return image

def display_box_instances_custom(image, boxes, masks, class_ids, class_name, scores, text_size, box_thickness, text_thickness):
    
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    txt_color = (255,255,255)
    assert boxes.shape[0] == masks.shape[-1] == class_ids.shape[0]

    for i, color in enumerate(colors):
        if not np.any(boxes[i]):
            continue

        y1, x1, y2, x2 = boxes[i]
        label = class_name[class_ids[i]]
        score = scores[i] if scores is not None else None
        caption = '{} {:.2f}'.format(label, score) if score else label
        mask = masks[:, :, i]
        
        image = apply_mask_custom(image, mask, color)
        color_rec = [int(c) for c in np.array(colors[i]) * 255]

        roi = image[y1:y2, x1:x2]
        blur = cv2.GaussianBlur(roi,(11,11),0)
        image[y1:y2, x1:x2] = blur
        #image = cv2.rectangle(image, (x1, y1), (x2, y2), color_rec, box_thickness)
        #image = cv2.putText(image, caption, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, text_size,  txt_color, text_thickness)
        cv2.imshow('Instance Segmentation', image)
        cv2.waitKey(0)
        
    return image

def get_mask_coordinates_arr(mask):
    # GET MASK COORDINATES
    contain_val = []

    mask_values = Mask(mask).polygons()
    val = mask_values.points
    contain_val.append(val)
    contain_val = np.asarray(contain_val, dtype = object)
    mask_coordinates = contain_val
    mask_coordinates_flat = mask_coordinates.flatten().tolist()
    #mask_coordinates_str = np.array2string(mask_coordinates_flat, precision=1, separator=',',suppress_small=True)

    if np.asarray(mask_coordinates_flat).shape[0] == 2:
        return mask_coordinates_flat[-1].flatten().tolist()

    """
    for a in range(mask.shape[2]):
    m = mask[:,:,a]
    mask_values = Mask(m).polygons()
    val = mask_values.points
    contain_val.append(val)

    contain_val = np.asarray(contain_val, dtype = object)
    mask_coordinates = contain_val
    """

    return mask_coordinates_flat

def get_mask_area(frame,mask,h,w):
    frame[mask] = 255
    frame[~mask] = 0
    unique, counts = np.unique(frame, return_counts=True)
    mask_area = list()
    mask_area.append(str((counts[1] / (counts[0] + counts[1]))*h*w))
    mask_area = (counts[1] / (counts[0] + counts[1]))*h*w

    # ALSO WORKS
    #positive_pixel_count = mask.sum() # assumes binary mask (True == 1)
    #area = positive_pixel_count / (w*h)
    #mask_area = area*h*w

    return mask_area

def get_mask_bbox(roi):
    x1 = roi[1]
    x2 = roi[3]
    y1 = roi[0]
    y2 = roi[2]

    x_top_left = x1
    y_top_left = y1
    x_length = x2 - x1
    y_height = y2 - y1

    return(x_top_left,y_top_left,x_length,y_height)

def get_json_list():
    with open('F:/Programming Project/7. Auto Image Extract/empty.json') as c:
      lines = c.readlines()

    annotation_json = json.loads(lines[0])
    return annotation_json

def append_annotation(annotations,mask_coordinates,x_top_left,y_top_left,x_length,y_height,mask_area,image_id):
    annotation = {
        "id": "NeedToGetFromJSONList_int",
        "iscrowd": 0,
        "image_id": image_id,
        "category_id": 1,
        "segmentation": [mask_coordinates],
        "bbox": [
            x_top_left,
            y_top_left,
            x_length,
            y_height
        ],
        "area": mask_area
    }
    annotations.append(annotation)