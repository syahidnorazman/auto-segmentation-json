import json
import ast

class Polygon:
    pass

class GenerateJSON:
  def generate_json(images,annotations):

    with open('F:/Programming Project/7. Auto Image Extract/empty.json') as c:
      lines = c.readlines()

    annotation_json = json.loads(lines[0])
    polygon = Polygon()

    polygon.info = annotation_json['info']
    polygon.images = annotation_json['images']
    polygon.annotations = annotation_json['annotations']
    polygon.categories = annotation_json['categories']

    for image in images:
      polygon.images.append(image)

    for annotation in annotations:
      polygon.annotations.append(annotation)

    #convert to JSON string
    #jsonStr = json.dumps(polygon.__dict__)
    #jsonStr = json.dumps(eval(str(polygon.__dict__)))
    jsonStr = json.dumps(ast.literal_eval(str(polygon.__dict__)))

    return jsonStr
