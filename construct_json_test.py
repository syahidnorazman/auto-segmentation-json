import json

class Polygon:
    pass

class GenerateJSON:
  def generate_json(description,images,annotations,categories):
    with open('F:/Programming Project/7. Auto Image Extract/git/auto-segmentation-json/test2.json') as c:
      lines = c.readlines()

    annotation_json = json.loads(lines[0])
    #return annotation_json['info']['description']

    polygon = Polygon()

    #polygon.info = { "description": description }
    #polygon.images = []
    #polygon.annotations = []
    #polygon.categories = []

    polygon.info = annotation_json['info']
    polygon.images = annotation_json['images']
    polygon.annotations = annotation_json['annotations']
    polygon.categories = annotation_json['categories']

    for image in images:
      polygon.images.append(image)

    for annotation in annotations:
      polygon.annotations.append(annotation)

    #for category in categories:
    #  polygon.categories.append(category)

    #convert to JSON string
    jsonStr = json.dumps(polygon.__dict__)

    return jsonStr
