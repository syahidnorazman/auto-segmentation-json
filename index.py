# python 3.9.9 | tensorflow/tensorflow-gpu 2.5.0
# pip install tensorflow==2.5.0 tensorflow-gpu==2.5.0 pixellib opencv-python

#import pixellib
#from pixellib.instance import instance_segmentation_custom

#import functions
#from functions import instance_segmentation

import functions
from functions.instance import instance_segmentation_custom
import cv2
import os

segmentation_model = instance_segmentation_custom()
segmentation_model.load_model('model/mask_rcnn_coco.h5')

files = os.listdir("datasets/")

for file in files:
    frame = cv2.imread("datasets/" + file)
    #cv2.imshow('Instance Segmentation', frame)
    #cv2.waitKey(0)

    res = segmentation_model.segmentFrame(frame, show_bboxes=True)